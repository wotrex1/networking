
import socket
import threading
import pyaudio
from tkinter import *
from tkinter import font
from tkinter import ttk
is_on = True
is_on_s = True
class Client:
    def __init__(self):
        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        
        while 1:
            try:
                self.target_ip = input('Enter IP address of server --> ')
                self.target_port = int(input('Enter target port of server --> '))

                self.s.connect((self.target_ip, self.target_port))

                break
            except:
                print("Couldn't connect to server")

        chunk_size = 1024 # 512
        audio_format = pyaudio.paInt16
        channels = 1
        rate = 20000
        # initialise microphone recording
        self.p = pyaudio.PyAudio()
        self.playing_stream = self.p.open(format=audio_format, channels=channels, rate=rate, output=True, frames_per_buffer=chunk_size)
        self.recording_stream = self.p.open(format=audio_format, channels=channels, rate=rate, input=True, frames_per_buffer=chunk_size)
        print("Connected to Server")
        # start threads
        thread1 = threading.Thread(target=self.window).start()
        receive_thread = threading.Thread(target=self.receive_server_data).start()
        self.send_data_to_server()
    def Switch_m(self):
        global is_on
          
        if is_on:

            self.label.config(text = "Microphone muted", 
                            fg = "grey")
            is_on = False
        else:
            
            self.label.config(text = "Microphone unmuted", fg = "green")
            is_on = True
    def Switch_s(self):
        global is_on_s
          
        if is_on_s:

            self.label2.config(text = "Sound muted", 
                            fg = "grey")
            is_on_s = False
        else:
            
            self.label2.config(text = "Sound unmuted", fg = "green")
            is_on_s = True
    def receive_server_data(self):
        while True:
            try:
                if is_on_s:
                    data = self.s.recv(1024)
                    self.playing_stream.write(data)
            except:
                pass
    def window(self):
        self.Window = Tk()
        self.Window.withdraw()
         

        self.login = Toplevel()
 
        self.login.title("VoiceChat")
        self.login.resizable(width = False,
                             height = False)
        self.login.configure(width = 400,
                             height = 300)
        self.label = Label(self.login,
                       text = "Microphone unmuted",
                       fg = "green",
                       font = "Helvetica 10 bold")
         
        self.label.place(relheight = 0.15,
                       relx = 0.1,
                       rely = 0.07)
        self.button = Button(self.login,
                         text = "Mute/unmute micr",
                         font = "Helvetica 10 bold",
                         command = lambda: self.Switch_m())
         
        self.button.place(relx = 0.1,
                      rely = 0.55)
        self.label2 = Label(self.login,
                       text = "Sound unmuted",
                       fg = "green",
                       font = "Helvetica 10 bold")
         
        self.label2.place(relheight = 0.15,
                       relx = 0.6,
                       rely = 0.07)
        self.button2 = Button(self.login,
                         text = "Mute/unmute sound",
                         font = "Helvetica 10 bold",
                         command = lambda: self.Switch_s())
         
        self.button2.place(relx = 0.6,
                      rely = 0.55)
        self.Window.mainloop()

    def send_data_to_server(self):
        while True:
            try:
                if is_on:
                    data = self.recording_stream.read(1024)
                    self.s.sendall(data)
            except:
                pass

client = Client()
