#include <limits.h>
#include "Window.h"
#include "WindowManager.h"

#if defined(__linux__)
#include <cstring>
#endif


FWindow::FWindow(const char*  WindowName,
	GLuint Width /* = 1280 */,
	GLuint Height /* = 720 */,
	GLuint ColourBits /* = 32 */, 
	GLuint DepthBits /* = 8 */,
	GLuint StencilBits /* = 8 */) :
	Name(WindowName),
	ColourBits(ColourBits),
	DepthBits(DepthBits),
	StencilBits(StencilBits)
{
	Resolution[0] = Width;
	Resolution[1] = Height;
	Position[0] = 0;
	Position[1] = 0;
	ShouldClose = GL_FALSE;
	EXTSwapControlSupported = GL_FALSE;
	SGISwapControlSupported = GL_FALSE;
	MESASwapControlSupported = GL_FALSE;

	if(!IsValidString(WindowName))
	{
		PrintErrorMessage(ERROR_INVALIDWINDOWNAME);
		exit(0);
	}

	InitializeEvents();

	CurrentState = WINDOWSTATE_NORMAL;
	ContextCreated = GL_FALSE;
	IsCurrentContext = GL_FALSE;
}


FWindow::~FWindow()
{
	Shutdown();
}


GLboolean FWindow::Shutdown()
{
	if(ContextCreated)
	{
		
#if defined (_WIN32) || defined(_WIN64)
		Windows_Shutdown();
#else
		Linux_Shutdown();
#endif
		ContextCreated = GL_FALSE;
		return FOUNDATION_OKAY;
	}

		PrintErrorMessage(ERROR_NOCONTEXT);
		return FOUNDATION_ERROR;
}


GLboolean FWindow::Initialize()
{
#if defined(_WIN32) || defined(_WIN64)
	return Windows_Initialize();
#else
	return Linux_Initialize();
#endif
}


GLboolean FWindow::GetShouldClose()
{
	return ShouldClose;
}


void FWindow::InitializeEvents()
{
	KeyEvent = nullptr;
	MouseButtonEvent = nullptr;
	MouseWheelEvent = nullptr;
	DestroyedEvent = nullptr;
	MaximizedEvent = nullptr;
	MinimizedEvent = nullptr;
//	RestoredEvent = nullptr;
	MovedEvent = nullptr;
	MouseMoveEvent = nullptr;
}



GLboolean FWindow::GetKeyState(GLuint Key)
{
	return Keys[Key];
}


GLboolean FWindow::InitializeGL()
{
#if defined(_WIN32) || (_WIN64)
	return Windows_InitializeGL();
#else
	return Linux_InitializeGL();
#endif
}


GLboolean FWindow::SwapDrawBuffers()
{
	if(ContextCreated)
	{
#if defined(_WIN32) || (_WIN64)
		SwapBuffers(DeviceContextHandle);
#else
		glXSwapBuffers(WindowManager::GetDisplay(), WindowHandle);
#endif
		return FOUNDATION_OKAY;
	}

	PrintErrorMessage(ERROR_NOCONTEXT);
	return FOUNDATION_ERROR;
}


GLboolean FWindow::SetSwapInterval(GLint SwapSetting)
{
	if(ContextCreated)
	{
	CurrentSwapInterval = SwapSetting;
#if defined(_WIN32) || defined(_WIN64)
	Windows_VerticalSync(SwapSetting);
#else
	Linux_VerticalSync(SwapSetting);
#endif

	return FOUNDATION_OKAY;
	}

	PrintErrorMessage(ERROR_NOCONTEXT);
	return FOUNDATION_ERROR;
}



GLuint FWindow::GetCurrentState()
{
	return CurrentState;
}


GLboolean FWindow::SetCurrentState(GLuint NewState)
{

	if(ContextCreated)
	{

	Restore();

	switch(NewState)
	{
		case WINDOWSTATE_MAXIMIZED:
			{
				Maximize(GL_TRUE);
				break;
			}

		case WINDOWSTATE_MINIMIZED:
			{
				Minimize(GL_TRUE);
				break;
			}

			case WINDOWSTATE_FULLSCREEN:
			{
				FullScreen(GL_FALSE);
				break;
			}

			default:
			{
				break;
			}
	}
	}

	PrintErrorMessage(ERROR_NOCONTEXT);
	return FOUNDATION_ERROR;
}


GLboolean FWindow::GetIsFullScreen()
{
	if(ContextCreated)
	{
		return (CurrentState == WINDOWSTATE_FULLSCREEN);
	}

	PrintErrorMessage(ERROR_NOCONTEXT);
	return FOUNDATION_ERROR;
}


GLboolean FWindow::FullScreen(GLboolean ShouldBeFullscreen)
{
	if(ContextCreated)
	{
		if (ShouldBeFullscreen)
		{
			CurrentState = WINDOWSTATE_FULLSCREEN;
		}

		else
		{
			CurrentState = WINDOWSTATE_NORMAL;
		}

#if defined(_WIN32) || defined(_WIN64)
		Windows_FullScreen();
#else
		Linux_FullScreen(ShouldBeFullscreen);
#endif

		return FOUNDATION_OKAY;
	}

	PrintErrorMessage(ERROR_NOCONTEXT);
	return FOUNDATION_OKAY;
}


GLboolean FWindow::GetIsMinimized()
{
	return (CurrentState == WINDOWSTATE_MINIMIZED);
}


GLboolean FWindow::Minimize(GLboolean NewState)
{
	if(ContextCreated)
	{
		if(NewState)
		{
			CurrentState = WINDOWSTATE_MINIMIZED;
		}

		else
		{
			CurrentState = WINDOWSTATE_NORMAL;
		}

#if defined(_WIN32) || defined(_WIN64)
	 	Windows_Minimize();
#else
	 	Linux_Minimize(NewState);
#endif

	 	return FOUNDATION_OKAY;
	}	

	return FOUNDATION_ERROR;
}


GLboolean FWindow::GetIsMaximized()
{
	return (CurrentState == WINDOWSTATE_MAXIMIZED) ;
}


GLboolean FWindow::Maximize(GLboolean NewState)
{
	if(ContextCreated)
	{
		if(NewState)
		{
			CurrentState = WINDOWSTATE_MAXIMIZED;
		}

		else
		{
			CurrentState = WINDOWSTATE_NORMAL;
		}

#if defined(_WIN32) || defined(_WIN64)
		 Windows_Maximize();
#else
	 	Linux_Maximize(NewState);
#endif
		 return FOUNDATION_OKAY;
	}
	PrintErrorMessage(ERROR_NOCONTEXT);	
	return FOUNDATION_ERROR;
}


GLboolean FWindow::Restore()
{
	if (ContextCreated)
	{
		switch (CurrentState)
		{
		case WINDOWSTATE_MAXIMIZED:
		{
			Maximize(GL_FALSE);
			break;
		}

		case WINDOWSTATE_FULLSCREEN:
		{
			FullScreen(GL_FALSE);
			break;
		}
		}

		CurrentState = WINDOWSTATE_NORMAL;
#if defined(_WIN32) || defined(_WIN64)
		Windows_Restore();
#else
		Linux_Restore();
#endif

		return FOUNDATION_OKAY;
	}

	PrintErrorMessage(ERROR_NOCONTEXT);
	return FOUNDATION_ERROR;
}


GLboolean FWindow::GetResolution(GLuint& Width, GLuint& Height)
{
	if (ContextCreated)
	{
		Width = Resolution[0];
		Height = Resolution[1];
		return FOUNDATION_OKAY;
	}

	PrintErrorMessage(ERROR_NOCONTEXT);
	return FOUNDATION_ERROR;
}


GLuint* FWindow::GetResolution()
{
	return Resolution;
}


GLboolean FWindow::SetResolution(GLuint Width, GLuint Height)
{
	if (ContextCreated)
	{
		if(Width > 0 && Height > 0)
		{
			Resolution[0] = Width;
			Resolution[1] = Height;

#if defined(_WIN32) || defined(_WIN64)
			Windows_SetResolution(Resolution[0], Resolution[1]);
#else
			Linux_SetResolution(Width, Height);	
#endif
			glViewport(0, 0, Resolution[0], Resolution[1]);

			return FOUNDATION_OKAY;
		}

		else
		{
			PrintErrorMessage(ERROR_INVALIDRESOLUTION);
			return FOUNDATION_ERROR;
		}
	}  

		PrintErrorMessage(ERROR_NOCONTEXT);
		return FOUNDATION_ERROR;
}



GLboolean FWindow::GetMousePosition(GLuint& X, GLuint& Y)
{
	if (ContextCreated)
	{
		X = MousePosition[0];
		Y = MousePosition[1];   /**< . */
		return FOUNDATION_OKAY;
	}

	PrintErrorMessage(ERROR_NOCONTEXT);
	return FOUNDATION_ERROR;
}



GLuint* FWindow::GetMousePosition()
{
	if (ContextCreated)
	{
		return MousePosition;
	}

	PrintErrorMessage(ERROR_NOCONTEXT);
	return nullptr;
}


GLboolean FWindow::SetMousePosition(GLuint X, GLuint Y)
{
	if (ContextCreated)
	{ 
		MousePosition[0] = X;
		MousePosition[1] = Y;
#if defined(_WIN32) || defined(_WIN64)
		Windows_SetMousePosition(X, Y);
#else
		Linux_SetMousePosition(X, Y);
#endif
		return FOUNDATION_OKAY;
	}

	PrintErrorMessage(ERROR_NOCONTEXT);
	return FOUNDATION_ERROR;
}


GLboolean FWindow::GetPosition(GLuint& X, GLuint& Y)
{
	if (ContextCreated)
	{
		X = Position[0];
		Y = Position[1];

		return FOUNDATION_OKAY;
	}

	PrintErrorMessage(ERROR_NOCONTEXT);
	return FOUNDATION_ERROR;
}


GLuint* FWindow::GetPosition()
{
	return Position;
}


GLboolean FWindow::SetPosition(GLuint X, GLuint Y)
{
	if (ContextCreated)
	{
		Position[0] = X;
		Position[1] = Y;
#if defined(_WIN32) || defined(_WIN64)
		Windows_SetPosition(Position[0], Position[1]);
#else
		Linux_SetPosition(X, Y);
#endif
	}

	PrintErrorMessage(ERROR_NOCONTEXT);
	return FOUNDATION_ERROR;
}



const char* FWindow::GetWindowName()
{
	if (ContextCreated)
	{
		return Name;
	}

	PrintErrorMessage(ERROR_NOCONTEXT);
	return nullptr;
}


GLboolean FWindow::SetTitleBar(const char* NewTitle)
{
	if (ContextCreated)
	{
		if(NewTitle != nullptr)
		{
#if defined(_WIN32) || defined(_WIN64)
			Windows_SetTitleBar(NewTitle);
#else
			Linux_SetTitleBar(NewTitle);
#endif
			return FOUNDATION_OKAY;
		}

		else
		{
			PrintErrorMessage(ERROR_INVALIDTITLEBAR);
			return FOUNDATION_ERROR;
		}
	}

	PrintErrorMessage(ERROR_NOCONTEXT);
	return FOUNDATION_ERROR;
}

GLboolean FWindow::SetIcon(const char* Icon, GLuint Width, GLuint Height)
{
	if (ContextCreated)
	{
#if defined(_WIN32) || defined(_WIN64)
		Windows_SetIcon(Icon, Width, Height);
#else
		Linux_SetIcon(Icon, Width, Height);
#endif
		return FOUNDATION_OKAY;
	}
	return FOUNDATION_ERROR;
}

GLboolean FWindow::SetStyle(GLuint WindowType)
{
	if (ContextCreated)
	{
#if defined(_WIN32) || defined(_WIN64)
		Windows_SetStyle(WindowType);
#else
		Linux_SetStyle(WindowType);
#endif
		PrintErrorMessage(ERROR_NOCONTEXT);
		return FOUNDATION_OKAY;
	}

	return FOUNDATION_ERROR;
}


GLboolean FWindow::MakeCurrentContext()
{
	if(ContextCreated)
	{
		IsCurrentContext = true;
#if defined(_WIN32) || defined(_WIN64)
		wglMakeCurrent(DeviceContextHandle, GLRenderingContextHandle);
#else
		glXMakeCurrent(WindowManager::GetDisplay(), WindowHandle, Context);
#endif
		return FOUNDATION_OKAY;
	}

	PrintErrorMessage(ERROR_NOCONTEXT);
	return FOUNDATION_ERROR;
}


GLboolean FWindow::GetIsCurrentContext()
{
	if(ContextCreated)
	{
		return IsCurrentContext;
	}
	PrintErrorMessage(ERROR_NOCONTEXT);
	return GL_FALSE;
}


GLboolean FWindow::GetContextHasBeenCreated()
{
	return ContextCreated;
}


void FWindow::InitGLExtensions()
{
#if defined(_WIN32) || defined(_WIN64)
	Windows_InitGLExtensions();
#else
	Linux_InitGLExtensions();
#endif
}

GLboolean FWindow::PrintOpenGLVersion()
{
	if(ContextCreated)
	{
		printf("%s\n", glGetString(GL_VERSION));
		return FOUNDATION_OKAY;
	}

	PrintErrorMessage(ERROR_NOCONTEXT);
	return FOUNDATION_ERROR;
}

const char* FWindow::GetOpenGLVersion()
{
	if(ContextCreated)
	{
		return (const char*)glGetString(GL_VERSION);
	}
	PrintErrorMessage(ERROR_NOCONTEXT);
	return nullptr;
}

GLboolean FWindow::PrintOpenGLExtensions()
{
	if(ContextCreated)
	{
		printf("%s \n", (const char*)glGetString(GL_EXTENSIONS));
		return FOUNDATION_OKAY;
	}

		PrintErrorMessage(ERROR_NOCONTEXT);
		return FOUNDATION_ERROR;
}


const char* FWindow::GetOpenGLExtensions()
{
	if(ContextCreated)
	{
		return (const char*)glGetString(GL_EXTENSIONS);
	}

	else
	{
		PrintErrorMessage(ERROR_NOCONTEXT);
		return nullptr;
	}
}

GLboolean FWindow::GetInFocus()
{
	return InFocus;
}

GLboolean FWindow::Focus(GLboolean ShouldBeInFocus)
{
	if (ContextCreated)
	{
		InFocus = ShouldBeInFocus;

#if defined(_WIN32) || defined(_WIN64)
		Windows_Focus();	
#else
		Linux_Focus(ShouldBeInFocus);
#endif
		return FOUNDATION_OKAY;
	}

	PrintErrorMessage(ERROR_NOCONTEXT);
	return FOUNDATION_ERROR;
}


GLboolean FWindow::SetOnKeyEvent(OnKeyEvent OnKey)
{
	if (IsValidKeyEvent(OnKey))
	{
		KeyEvent = OnKey;
		return FOUNDATION_OKAY;
	}

	return FOUNDATION_ERROR;
}


GLboolean FWindow::SetOnMouseButtonEvent(OnMouseButtonEvent OnMouseButtonEvent)
{
	//we don't really need to check if the context has been created
	if(IsValidKeyEvent(OnMouseButtonEvent))
	{
		MouseButtonEvent = OnMouseButtonEvent;
		return FOUNDATION_OKAY;
	}

	PrintErrorMessage(ERROR_INVALIDEVENT);
	return FOUNDATION_ERROR;
}


GLboolean FWindow::SetOnMouseWheelEvent(OnMouseWheelEvent OnMouseWheel)
{
	if(IsValidMouseWheelEvent(OnMouseWheel))
	{
		MouseWheelEvent = OnMouseWheel;
		return FOUNDATION_OKAY;
	}

	PrintErrorMessage(ERROR_INVALIDEVENT);
	return FOUNDATION_ERROR;
}


GLboolean FWindow::SetOnDestroyed(OnDestroyedEvent OnDestroyed)
{
	if(IsValidDestroyedEvent(OnDestroyed))
	{
		DestroyedEvent = OnDestroyed;
		return FOUNDATION_OKAY;
	}

	PrintErrorMessage(ERROR_INVALIDEVENT);
	return FOUNDATION_ERROR;
}


GLboolean FWindow::SetOnMaximized(OnMaximizedEvent OnMaximized)
{
	if(IsValidDestroyedEvent(OnMaximized))
	{
		MaximizedEvent = OnMaximized;
		return FOUNDATION_OKAY;
	}
	PrintErrorMessage(ERROR_INVALIDEVENT);
	return FOUNDATION_ERROR;
}


GLboolean FWindow::SetOnMinimized(OnMinimizedEvent OnMinimized)
{
	if(IsValidDestroyedEvent(OnMinimized))
	{
		MinimizedEvent = OnMinimized;
		return FOUNDATION_OKAY;
	}

	PrintErrorMessage(ERROR_INVALIDEVENT);
	return FOUNDATION_ERROR;
}

/*void FWindow::SetOnRestored(OnRestoredEvent OnRestored)
{
	if (IsValid(OnRestored))
	{
		RestoredEvent = OnRestored;
	}
}*/


GLboolean FWindow::SetOnFocus(OnFocusEvent OnFocus)
{
	if(IsValidFocusEvent(OnFocus))
	{
		FocusEvent = OnFocus;
		return FOUNDATION_OKAY;
	}

	PrintErrorMessage(ERROR_INVALIDEVENT);
	return FOUNDATION_ERROR;
}


GLboolean FWindow::SetOnMoved(OnMovedEvent OnMoved)
{
	if(IsValidMovedEvent(OnMoved))
	{
		MovedEvent = OnMoved;
		return FOUNDATION_OKAY;
	}
	PrintErrorMessage(ERROR_INVALIDEVENT);
	return FOUNDATION_ERROR;
}


GLboolean FWindow::SetOnResize(OnResizeEvent OnResize)
{
	if(IsValidMovedEvent(OnResize))
	{
		ResizeEvent = OnResize;
		return FOUNDATION_OKAY;
	}

	PrintErrorMessage(ERROR_INVALIDEVENT);
	return FOUNDATION_ERROR;
}


GLboolean FWindow::SetOnMouseMove(OnMouseMoveEvent OnMouseMove)
{
	if(IsValidMouseMoveEvent(OnMouseMove))
	{
		MouseMoveEvent = OnMouseMove;
		return FOUNDATION_OKAY;
	}

	PrintErrorMessage(ERROR_INVALIDEVENT);
	return FOUNDATION_ERROR;
}

GLboolean FWindow::EnableDecorator(GLbitfield Decorator)
{
	if (ContextCreated)
	{
#if defined(_WIN32) || defined(_WIN64)
		Windows_EnableDecorator(Decorator);
#else
		Linux_EnableDecorator(Decorator);
#endif

		return FOUNDATION_OKAY;
	}
	PrintErrorMessage(ERROR_NOCONTEXT);
	return FOUNDATION_ERROR;
}

GLboolean FWindow::DisableDecorator(GLbitfield Decorator)
{
	if (ContextCreated)
	{
#if defined(_WIN32) || defined(_WIN64)
		Windows_DisableDecorator(Decorator);
#else
		Linux_DisableDecorator(Decorator);
#endif
		return FOUNDATION_OKAY;
	}

	PrintErrorMessage(ERROR_NOCONTEXT);
	return FOUNDATION_ERROR;
}
