#include "server.h"

void handle_connection(SOCKET socket, sockaddr_in *addr) {
    char *str_in_addr = inet_ntoa(addr->sin_addr);
    printf("[%s]>>%s\n", str_in_addr, "Establish new connection");
    int rc = 1;
    while (rc > 0) {
        PolinomRequest request;
        memset(&request, sizeof(request), 0);

        PolinomResponse response;
        memset(&request, sizeof(response), 0);

        recv(socket, (char *) &request, sizeof(request), 0);
        compute(&request, &response);
        rc = send(socket, (char *) &response, sizeof(response), 0);
    }
    close_socket(socket);
    printf("[%s]>>%s", str_in_addr, "Close incoming connection");
}

PolinomResponse *compute(PolinomRequest *request, PolinomResponse *response) {
	response->x = request->x;
	response->a[0] = request->a[0];
	response->n = request->n;

	response->number = calculate(request->n, request->x, request->a);
	return response;
}

int calculate(int n, int x, int* a[1]) {
	const int nn = n + 1;
    int* arr = new int[nn];
	arr = a[0];
	printf("%d %d", arr[0], arr[1]);
    int result = 0;
    for (int i = 0; i < n+1; i++) {
        if (i == 0) {
            result = arr[0];
        }
        else {
            result *= x;
            result += arr[i];
        }
    }
    return result;
}
