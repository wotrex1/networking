#include "client.h"

void exit_handler();

SOCKET client_socket;

void process_response(PolinomResponse *);

int main(int argc, char *argv[]) {
    atexit(common_exit_handler);
    atexit(exit_handler);

    short port;
    char host[128] = "";
    bool parse_cmd_result = parse_cmd(argc, argv, host, &port);

    if (!parse_cmd_result || !host || !strlen(host)) {
        printf("Invalid host or port. Usage %s -h host -p port\n", argv[0]);
        return -1;
    }

    common_init_handler();

    client_socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (client_socket <= 0) {
        error_msg("Can't create socket");
        return -1;
    }

    struct sockaddr_in server_addr;
    init_inet_address(&server_addr, host, port);

    //Connect to the server
    if (connect(client_socket, (sockaddr *) &server_addr, sizeof(sockaddr))) {
        char err_msg[128] = "";
        sprintf(err_msg, "Can't connect to the server %s:%d", host, port);
        error_msg(err_msg);
        return -1;
    }

    printf("Connection to the server %s:%d success\n", host, port);

    PolinomRequest request;
    PolinomResponse response;
    {
        printf("%s\n", "Enter number x");
        scanf("%d", &request.x);
        getchar();
		printf("%s\n", "Enter n sequence");
        scanf("%d", &request.n);
        getchar();
        const int nn = *&request.n + 1;
		int* arr = new int[nn];
		for(int i = 0; i < *&request.n + 1; i++){
		    printf("Enter a coefficient %d \n",i+1);
            scanf("%d", &arr[i]);
            printf("%d\n", arr[i]);
            getchar();
		}
        request.a[0] = arr;
        // int* arr2 = new int[nn];
        // arr2 = request.a[0];
        // printf("%d %d %d",arr2[0], arr2[1], arr2[2]);
        int sc = send(client_socket, (char *) &request, sizeof(request), 0);
        if (sc <= 0) {
            char err_msg[128] = "";
            sprintf(err_msg, "Can't send data to the server %s:%d", host, port);
            error_msg(err_msg);
            return -1;
        }
    }

    {
        int sc = recv(client_socket, (char *) &response, sizeof(response), 0);
        if (sc <= 0) {
            char err_msg[128] = "";
            sprintf(err_msg, "Can't received data from the server %s:%d", host, port);
            error_msg(err_msg);
            return -1;
        }

        process_response(&response);
    }

    return 0;
}

void process_response(PolinomResponse *res) {
	 const int nn = res->n + 1;
	 int* arr = new int[nn];
	 arr = res->a[0];
	 printf("Number X: %d\n", res->x);
	 printf("Coefficients a:");
	 for(int i = 0; i < res->n + 1; i++){
		 printf(" %d",arr[i]);
	 }
	printf("\nSequence n: %d\n", res->n);
	printf("Polinom: a0xn+a1xn-1+a2xn-2+...+an \n");
	//printf("%d0xn+a1xn-1+a2xn-2+...+an \n",res->a[1]);
    printf("Result: %d", res->number);
}

void exit_handler() {
    close_socket(client_socket);
}
