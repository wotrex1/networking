#include "client.h"

void exit_handler();

void process_response(PolinomResponse *rs);

THREAD_RESULT process_connection(void *data) {
    PCLIENT_OPTIONS poptions = (PCLIENT_OPTIONS) data;
    struct sockaddr_in server_addr;
    common_init_handler();
    SOCKET client_socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (client_socket <= 0) {
        error_msg("Can't create socket");
        close_socket(client_socket);
        return -1;
    }

    init_inet_address(&server_addr, poptions->server_host, poptions->server_port);
    //Connect to the server
    if (connect(client_socket, (sockaddr *) &server_addr, sizeof(sockaddr))) {
        char err_msg[128] = "";
        sprintf(err_msg, "Can't connect to the server %s:%d", poptions->server_host, poptions->server_port);
        error_msg(err_msg);
        close_socket(client_socket);
        return -1;
    }
    printf("Connection to the server %s:%d success\n",
           poptions->server_host,
           poptions->server_port);

    PolinomRequest request;
    request.x = 3;
	request.n = 4;
    memcpy(request.text, poptions->data, sizeof(poptions->data));
    {
        printf("Sending %d, %d \n", request.x, request.n);
        int sc = send(client_socket, (char *) &request, sizeof(request), 0);
        if (sc <= 0) {
            char err_msg[128] = "";
            sprintf(err_msg, "Can't send data to the server %s:%d", poptions->server_host, poptions->server_port);
            error_msg(err_msg);
            close_socket(client_socket);
            return -1;
        }
    }

    PolinomResponse response;
    {
        int sc = recv(client_socket, (char *) &response, sizeof(response), 0);
        if (sc <= 0) {
            char err_msg[128] = "";
            sprintf(err_msg, "Can't receive data from the server %s:%d", poptions->server_host, poptions->server_port);
            error_msg(err_msg);
            close_socket(client_socket);
            return -1;
        }
        process_response(&response);
    }

    close_socket(client_socket);
    return 0;
}

void process_response(PolinomResponse *res) {
     const int nn = res->n + 1;
	 int* arr = new int[nn];
	 arr = res->a[0];
	 printf("Number X: %d\n", res->x);
	 printf("Coefficients a:");
	 for(int i = 0; i < res->n + 1; i++){
		 printf(" %d", arr[i]);
	 }
	printf("\nSequence n: %d\n", res->n);
	printf("Polinom: a0xn+a1xn-1+a2xn-2+...+an \n");
	//printf("%d0xn+a1xn-1+a2xn-2+...+an \n",res->a[1]);
    printf("Result: %d\n", res->number);
}
