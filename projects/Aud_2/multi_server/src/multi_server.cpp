#include "multi_server.h"

THREAD_RESULT handle_connection(void *data) {
    SOCKET socket = (SOCKET) data;
//    CHECK_IO((socket = *((SOCKET *) data)) > 0, (THREAD_RESULT) - 1, "Invalid socket\n");
    struct sockaddr_in addr;
    socklen_t addr_len = sizeof(addr);
    if (getpeername(socket, (sockaddr *) &addr, &addr_len)) {
        printf("Error retrieving peer info\n");
        return -1;
    }
    char *str_in_addr = inet_ntoa(addr.sin_addr);
    printf("[%s]>>%s\n", str_in_addr, "Establish new connection");
    int rc = 1;
    while (rc > 0) {
        PolinomRequest request;
        memset(&request, sizeof(request), 0);

        PolinomResponse response;
        memset(&request, sizeof(response), 0);

        recv(socket, (char *) &request, sizeof(request), 0);
        compute(&request, &response);
        rc = send(socket, (char *) &response, sizeof(response), 0);
    }
    printf("[%s]>>%s", str_in_addr, "Close incoming connection\n");
    return 0;
}

PolinomResponse *compute(PolinomRequest *request, PolinomResponse *response) {
    response->x = request->x;
	//response->a[0] = request->a[0];
    int arr[5] = { 2, 3, 4, 5, 6 };
    response->a[0] = arr;
	response->n = request->n;

	response->number = calculate(request->n, request->x, request->a);
	return response;
}

int calculate(int n, int x, int* a[1]) {
	//const int nn = n + 1;
   // int* arr = new int[nn];
	//arr = a[0];
	//printf("%d %d", arr[0], arr[1]);
    int result = 0;
    int arr[5] = { 2, 3, 4, 5, 6 };
    for (int i = 0; i < n+1; i++) {
        if (i == 0) {
            result = arr[0];
        }
        else {
            result *= x;
            result += arr[i];
        }
    }
    return result;
}